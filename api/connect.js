import request,{Method} from '../utils/request'
import md5Libs from '../uview-ui/libs/function/md5.js'
import {Keys} from '@/utils/cache.js';

//读取唯一的UUID
const uuid = uni.getStorageSync(Keys.uuid);

/**
 * 账户密码登录绑定
 * @param uuid
 */
export function loginBindConnectByAccount(uuid, params) {
  const _params = { ...params }
  _params.password = md5Libs.md5(_params.password)
  return request.ajax({
    url: `/buyer/passport/login-binder/wap/${uuid}`,
    method: Method.POST,
    params: _params
  })
}

/**
 * 注册绑定
 * @param uuid
 * @param params
 */
export function registerBindConnect(uuid,params) {
  const _params = { ...params }
  _params.password = md5Libs.md5(_params.password)
  return request.ajax({
    url: `/buyer/passport/mini-program/register-bind/${uuid}`,
    method: Method.POST,
    params:_params
  })
}

/**
 * 微信小程序自动登录
 */
export function loginByAuto(params) {
  return request.ajax({
    url: '/buyer/passport/mini-program/auto-login',
    method: Method.GET,
    loading: true,
    params
  })
}

/**
 * 加密数据解密验证
 * @param params
 */
export function accessUnionID(params) {
  return request.ajax({
    url: '/buyer/passport/mini-program/decrypt',
    method: Method.GET,
    loading: true,
    params
  })
}

/**
 * 发送第三方登录手机验证码
 * @param mobile
 * @param captcha
 * @param uuid
 */
export function sendMobileLoginSms(mobile, captcha, uuid) {
  return request.ajax({
    url: `/buyer/passport/mobile-binder/sms-code/${mobile}`,
    method: Method.POST,
    params: {
      captcha,
      uuid
    }
  })
}

/**
 * 第三方登录绑定【通过手机验证码方式登录】
 * @param uuid
 * @param params
 */
export function loginByMobileConnect(uuid, params) {
  return request.ajax({
    url: `/buyer/passport/mobile-binder/${uuid}`,
    method: Method.POST,
    params
  })
}


/**
 * 获取绑定列表
 */
export function getConnectList() {
  return request.ajax({
    url: '/buyer/account-binder/list',
    method: Method.GET,
    needToken: true
  })
}

/**
 * 获取个人中心绑定url
 * @param login_type
 * @returns {string}
 */
export function getLogindConnectUrl(login_type) {
  return request.ajax({
    url: `/buyer/account-binder/pc/${login_type}`,
    method: Method.GET,
    needToken: true
  })
}

/**
 * 解绑
 * @param type
 */
export function unbindConnect(type) {
  return request.ajax({
    url: `/buyer/account-binder/unbind/${type}`,
    method: Method.POST,
    needToken: true
  })
}

/**
 * app微信授权登陆
 * @param uuid
 * @param params
 */
export function wxAppLogin(uuid, params) {
  return request.ajax({
    url: `/buyer/wechat/app/login/${uuid}`,
    method: Method.POST,
    params
  })
}

/**
 * 微信小程序授权登陆
 * @param uuid
 * @param params
 */
export function wxMiniLogin(params) {
  return request.ajax({
    url: `/buyer/wechat/mini/login`,
    method: Method.POST,
    params
  })
}


/**
 * H5获取授权页地址
 * @param redirectUri 授权成功后跳转地址
 */
export function wxH5GetLoginUrl(redirectUri) {
  return request.ajax({
    url: `/buyer/wechat/wap/getLoginUrl`,
    method: Method.GET,
    params:{redirectUri}
  })
}

/**
 * 微信H5登陆
 * @param code
 * @param uuid
 */
export function wxH5Login(code,uuid,oldUuid) {
  return request.ajax({
    url: `/buyer/wechat/wap/login`,
    method: Method.GET,
    params:{code,uuid,oldUuid}
  })
}

/**
 * 绑定手机号
 * @param ask_id
 * @param reply_content
 * @param anonymous
 */
export function wxMiniBindPhone(encrypted, iv) {
  return request.ajax({
    url: `/buyer/wechat/mini/bind/phone`,
    method: 'post',
    needToken: true,
    params:{encrypted,iv}
  })
}

/**
 * H5 QQ登陆
 * @param code
 * @param uuid
 */
export function qqH5Login(access_token,uuid) {
  return request.ajax({
    url: `/buyer/connect/qq/wap/login`,
    method: Method.GET,
    params:{access_token,uuid}
  })
}

/**
 * H5 获取appid
 * @param code
 * @param uuid
 */
export function getQQAppid() {
  return request.ajax({
    url: `/buyer/connect/qq/wap/getAppid`,
    method: Method.GET
  })
}

/**
 * app QQ授权登陆
 * @param uuid
 * @param params
 */
export function qqAppLogin(uuid, params) {
  return request.ajax({
    url: `/buyer/connect/qq/app/login/${uuid}`,
    method: Method.POST,
    params
  })
}

/**
 * 获取支付宝登陆跳转页地址
 * @param uuid
 * @param params
 */
export function aliPayGetLoginUrl(redirectUri) {
  return request.ajax({
    url: `/buyer/connect/alipay/wap/getLoginUrl`,
    method: Method.GET,
    params:{redirectUri}
  })
}

/**
 * H5 支付宝登陆
 * @param code
 * @param uuid
 */
export function alipayH5Login(code,uuid) {
  return request.ajax({
    url: `/buyer/connect/alipay/wap/login`,
    method: Method.GET,
    params:{code,uuid}
  })
}

/**
 * 获取微博登陆跳转页地址
 * @param code
 * @param uuid
 */
export function weiboGetLoginUrl(redirectUri) {
  return request.ajax({
    url: `/buyer/connect/weibo/wap/getLoginUrl`,
    method: Method.GET,
    params:{redirectUri}
  })
}

/**
 * H5 微博登陆
 * @param code
 * @param uuid
 */
export function weiboH5Login(params) {
  return request.ajax({
    url: `/buyer/connect/weibo/wap/login`,
    method: Method.POST,
    params
  })
}

/**
 * app QQ授权登陆
 * @param uuid
 * @param params
 */
export function weiboAppLogin(params) {
  return request.ajax({
    url: `/buyer/connect/weibo/app/login`,
    method: Method.POST,
    params
  })
}

/**
 * app appleID授权登陆
 * @param uuid
 * @param params
 */
export function appleAppLogin(params) {
  return request.ajax({
    url: `/buyer/connect/apple/app/login/${uuid}`,
    method: Method.POST,
    params
  })
}