/**
 * Created by Andste on 2018/5/2.
 * 用户认证相关API
 */

import request,{Method} from '../utils/request'
import { api } from '../config/config'
import md5Libs from "@/uview-ui/libs/function/md5";
import {Keys} from '@/utils/cache.js';

//读取唯一的UUID
const uuid = uni.getStorageSync(Keys.uuid);

/**
 * 普通登录
 * @param params
 */
export function login(params) {
  params = JSON.parse(JSON.stringify(params))
  params.password = md5Libs.md5(params.password)
  return request.ajax({
    url: `/buyer/passport/login`,
    method: Method.POST,
    loading: true,
    params
  })
}

/**
 * 通过手机号登录
 * @param mobile
 * @param sms_code
 */
export function loginByMobile(params) {
  return request.ajax({
    url: `/buyer/passport/login/${params.mobile}`,
    method: Method.POST,
    loading: true ,
    params: {
        scene: params.scene,
        mobile: params.mobile,
        captcha: params.captcha,
        sms_code: params.sms_code,
        uuid
    }
  })
}

/**
 * 发送会员注册手机验证码
 * @param mobile
 * @param captcha
 */
export function sendRegisterSms(mobile, captcha) {
  return request.ajax({
    url: `/buyer/passport/register/smscode/${mobile}`,
    method: Method.POST,
    loading: true,
    params: {
      captcha,
      uuid,
	  scene:'REGISTER'
    }
  })
}

/**
 * 发送会员登录手机验证码
 * @param mobile
 * @param captcha
 */
export function sendLoginSms(params) {
  return request.ajax({
    url: `/buyer/passport/login/smscode/${params.mobile}`,
    loading: false,
    method: Method.POST,
    params: {
        scene: params.scene,
        mobile: params.mobile,
        captcha: params.captcha,
        sms_code: params.sms_code,
        uuid
    }
  })
}

/**
 * 用户名重复校验
 * @param username
 */
export function checkUsernameRepeat(username) {
  return request.ajax({
    url: `/buyer/passport/username/${username}`,
    method: Method.GET
  })
}

/**
 * 手机号重复校验
 * @param mobile
 */
export function checkMobileRepeat(mobile) {
  return request.ajax({
    url: `/buyer/passport/mobile/${mobile}`,
    method: Method.GET
  })
}

/**
 * 注册会员【手机号】
 * @param mobile
 * @param password
 */
export function registerByMobile(mobile, password) {
  return request.ajax({
    url: '/buyer/passport/register/wap',
    method: Method.POST,
    params: {
      mobile,
      password: md5Libs.md5(password)
    }
  })
}

/**
 * 验证手机验证码
 * @param mobile   手机号码
 * @param scene    业务场景
 * @param sms_code 短信验证码
 */
export function validMobileSms(mobile, scene, sms_code) {
  return request.ajax({
    url: `/buyer/passport/smscode/${mobile}`,
    method: Method.GET,
    loading: true ,
    params: {
      scene,
      sms_code
    }
  })
}

/**
 * 验证账户信息
 * @param uuid
 * @param captcha
 * @param account
 */
export function validAccount(uuid, captcha, account,scene) {
  return request.ajax({
    url: `/buyer/passport/find-pwd`,
    method: Method.GET,
    loading: true,
    params: {
      uuid,
      captcha,
      account,
      scene
    }
  })
}

/**
 * 发送找回密码短信
 * @param uuid
 * @param captcha
 */
export function sendFindPasswordSms(uuid, captcha,scene) {
  return request.ajax({
    url: `/buyer/passport/find-pwd/send`,
    method: Method.POST,
    loading:true,
    params: {
      uuid,
      captcha,
      scene
    }
  })
}

/**
 * 校验找回密码验证码
 * @param uuid
 * @param sms_code
 */
export function validFindPasswordSms(uuid, sms_code) {
  return request.ajax({
    url: `/buyer/passport/find-pwd/valid`,
    method: Method.GET,
    loading:true,
    params: {
      uuid,
      sms_code
    }
  })
}

/**
 * 修改密码【找回密码用】
 * @param uuid
 * @param password
 */
export function changePassword(uuid, password) {
  return request.ajax({
    url: `/buyer/passport/find-pwd/update-password`,
    method: 'put',
    loading: true,
    params: {
      uuid,
      password: md5Libs.md5(password)
    }
  })
}
