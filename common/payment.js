/**
 * 支付相关方法
 */

/**
 * 微信小程序支付
 * @param {function} paymentParams 支付参数
 * @param {function} paySuccess 支付成功回调
 * @param {function} payFail 支付失败回调
 */
export function wechatMiniPay(paymentParams,paySuccess,payFail) {
    uni.requestPayment({
        provider: 'wxpay',
        timeStamp: paymentParams.timeStamp,
        nonceStr: paymentParams.nonceStr,
        package: paymentParams.package,
        signType: paymentParams.signType,
        paySign: paymentParams.paySign,
        success(res) {
            console.log('success:' + JSON.stringify( res ));
            paySuccess(res)
        },
        fail: function (err) {
            payFail(err)
            console.log('fail:' + JSON.stringify(err));
        }
    })
}



/**
 * 微信App支付
 * @param {function} paymentParams 支付参数
 * @param {function} paySuccess 支付成功回调
 * @param {function} payFail 支付失败回调
 */
export function wechatAppPay(orderInfo,paySuccess,payFail) {
    console.log("weixin app call1")
    uni.requestPayment({
        provider: 'wxpay',
        orderInfo:orderInfo,
        success(res) {
            console.log('success:' + JSON.stringify(res) );
            paySuccess(res)
        },
        fail: function (err) {
            payFail(err)
			console.log("fail", err);
        }
    })
}



/**
 * 手机浏览器h5支付
 * @param {Object} paymentParams
 * @param {Object} paySuccess
 * @param {Object} payFail
 */
export function mobileH5Pay(response,paySuccess,payFail) {
    location.href = response.gateway_url

}
/**
 * 微信内嵌h5支付
 * @param {Object} paymentParams
 * @param {Object} paySuccess
 * @param {Object} payFail
 */
export function wechatH5Pay(paymentParams,paySuccess,payFail) {
       WeixinJSBridge.invoke(
          'getBrandWCPayRequest', {
             "appId":paymentParams.appId,     //公众号名称，由商户传入
             "timeStamp":paymentParams.timeStamp,        //时间戳，自1970年以来的秒数
             "nonceStr":paymentParams.nonceStr, //随机串
             "package":paymentParams.package,
             "signType": paymentParams.signType,        //微信签名方式
             "paySign":paymentParams.paySign //微信签名
          },
          function(res){
            paySuccess(res)
            if(res.err_msg == "get_brand_wcpay_request:ok" ){

            }
       });
}

/**
 * 支付宝H5支付
 * 在
 * @param {Object} response
 * @param {Object} paySuccess
 * @param {Object} payFail
 */
export function aliPayH5(response, paySuccess,payFail) {
    let $formItems = ''

    let { form_items, gateway_url } = response
    if (typeof form_items === 'string' && form_items.indexOf('[') === 0) {
        form_items = JSON.parse(form_items)
    }
    form_items.forEach(item => {
        $formItems += `<input name="${item.item_name}" type="hidden" value='${item.item_value}' />`
    })
    let $form = `<form action="${gateway_url}" method="post">${$formItems}</form>`
    document.getElementsByClassName('__pay_form__')[0].innerHTML = $form
    document.forms[0].submit()
}

export function aliPayApp(response,paySuccess,payFail) {
    uni.requestPayment({
        provider: 'alipay',
        orderInfo: response.gateway_url, //支付宝订单数据
        success: function (res) {
            console.log('success:' + JSON.stringify(res));
            paySuccess()
        },
        fail: function (err) {
            console.log('fail:' + JSON.stringify(err));
        }
    });
}
