# ShopTnt B2B2C 快速上手文档

## 演示

### 演示地址

平台管理端：https://manager-bbc.shoptnt.cn 账号：admin/111111

店铺管理端：https://seller-bbc.shoptnt.cn 账号：ceshi/111111

商城PC页面：https://shop-bbc.shoptnt.cn 账号可自行注册

商城移动端，扫描如下二维码

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/weichat-gongzhonghao.jpg?x-oss-process=style/300x300)

## 交流、反馈

### 推荐
官方微信群：

![](https://shoptnt-statics.oss-cn-beijing.aliyuncs.com/qun.png?x-oss-process=style/300x300)

## 使用须知

1. 允许个人学习使用。
2. 允许用于学习、毕业设计等。
3. 禁止将本开源的代码和资源进行任何形式任何名义的出售。
4. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 或微信扫一扫加我微信。

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/danren.png?x-oss-process=style/300x300)


## 开发语言

* 基于vue语言开发
* uni-app  https://uniapp.dcloud.io/
* UI框架    https://www.uviewui.com/

## 快速上手帮助

#### 1、公用组件

目录：pages/demo/demo.vue


#### 2、配置说明

​	(2.1)目录：config/config.js

​	(2.2)目录： manifest.json 

​		（2.2.1）微信小程序直播组件配置：https://www.cnblogs.com/xiaoxiao2017/p/13476505.html

​		（2.2.2）canvas h5跨域： https://www.jianshu.com/p/a05a89b56b5c


#### 3、JS工具目录

目录：utils/


#### 4、HTTP 封装文件

目录：utils/request.js


#### 5、缓存管理

目录：utils/cache.js


## 兼容注意事项

### 1、所有的像素单位必须使用rpx,除非极其特殊的情况，需报备。
### 2、所有布局必须使用flex布局
### 3、开发时必须基于微信小程序进行开发，基本上小程序样式正常，其它端样式基本都是正常的。
### 4、目前不支持nvue
### 5、所有页面不要用 scroll-view 组件。可以使用 mescroll-uni 代替。参考页面：订单列表，购物车等页面。
### 6、公用组件必须在 目录：pages/demo/demo.vue 写上例子。
### 7、读写缓存必须使用 utils/cache.js 里面的方法。
### 8、开发时：manifest.json 配置文件中的 微信小程序不需要配置 APPID。开发调试微信登录，支付的时候再配置。
### 9、组件事件传递的时候不要直接在事件上赋值 (比如：@num-changed="(num) => { buyNum = num }")
