const env = 'deveploment' // 开发环境 deveploment  生产环境production

/**
 * base    : 基础业务API
 * buyer   : 买家API
 */
const api = {
  // 开发环境
  dev: {
    base: 'https://base-bbc-api.shoptnt.cn',
    buyer: 'https://shop-bbc-api.shoptnt.cn',
    im: 'https://api.shoptnt.cn/im'
  },
  // 生产环境
  pro: {
    base: 'https://base-bbc-api.shoptnt.cn',
    buyer: 'https://shop-bbc-api.shoptnt.cn',
    im: 'https://api.shoptnt.cn/im'
  }
}
/**
 * wap: m站域名
 */
const domain = {
  //开发环境
  dev: {
    wap: 'https://uniapp.shoptnt.cn',
    seller: 'https://seller-v722.shoptnt.cn'
  },
  //生成环境
  pro: {
    wap: 'https://m-buyer.shoptnt.cn',
    seller: 'https://seller.shoptnt.cn'
  }

}
module.exports = {
  api: env === 'deveploment' ? api.dev : api.pro,
  domain: env === 'deveploment' ? domain.dev : domain.pro,
  env,
  /**
   * 直播功能开关
   * 如果您需要开启直播相关功能，请设置为true
   * 微信小程序直播组件配置：https://www.cnblogs.com/shoptnt-docs/p/13954116.html
   */
  liveVideo: false,
  /**
   * 是否开启IM功能
   */
  im: false,
  /**
   * 是否使用汇付支付
   */
  huiFuPay: false
}
